﻿using System.Collections.Generic;
using RimWorld;
using RimWorld.Planet;
using UnityEngine;
using Verse;

namespace NMeijer.SettlementInventory
{
	public class Dialog_BuyableItems : Window
	{
		#region Consts

		private const float LabelHeight = 24f;

		private static readonly Vector2 BottomButtonSize = new Vector2(160f, 40f);

		#endregion

		#region Variables

		private readonly Settlement _settlement;
		private readonly bool _commsConsoleRequirementFulfilled;
		private readonly bool _visitRequirementFulfilled;

		private readonly List<TabRecord> _tabs = new List<TabRecord>();
		private Vector2 _scrollPosition;

		private ThingCategoryDef _currentCategory;
		private readonly List<ThingDef> _categoryItems = new List<ThingDef>();

		#endregion

		#region Properties

		public override Vector2 InitialSize => new Vector2(650f, CanShowInventory ? Mathf.Min(UI.screenHeight, 1000) : 300f);

		protected override float Margin => 0.0f;

		private bool CanShowInventory => _visitRequirementFulfilled && _commsConsoleRequirementFulfilled;

		#endregion

		public Dialog_BuyableItems(Settlement settlement)
		{
			_settlement = settlement;
			_visitRequirementFulfilled = !SettlementInventoryMod.Settings.RequireFirstVisit || _settlement.EverVisited;
			_commsConsoleRequirementFulfilled = !SettlementInventoryMod.Settings.RequireCommsConsole || CommsConsoleUtility.PlayerHasPoweredCommsConsole();

			forcePause = true;
			absorbInputAroundWindow = true;

			// Tabs are only shown if the player has visited this settlement.
			// Showing the goods without having visited would cause it to generate them, which would in turn start the restock countdown.
			if(CanShowInventory)
			{
				CalculateTabs();
			}
		}

		#region Public Methods

		public override void DoWindowContents(Rect inRect)
		{
			float currentTopHeight = 40f;
			Rect rect1 = new Rect(0.0f, 0.0f, inRect.width, currentTopHeight);
			Text.Font = GameFont.Medium;
			Text.Anchor = TextAnchor.MiddleCenter;

			TaggedString label = "BuyableItemsTitle".Translate();
			Widgets.Label(rect1, label);

			Text.Font = GameFont.Small;

			Text.Anchor = TextAnchor.MiddleCenter;

			Rect labelRect = new Rect(0f, 40f, inRect.width, LabelHeight);

			if(!_settlement.EverVisited)
			{
				Widgets.Label(labelRect, "TraderNotVisitedYet".Translate());
			}
			else if(_settlement.RestockedSinceLastVisit)
			{
				Widgets.Label(labelRect, "TraderRestockedSinceLastVisit".Translate());
			}
			else
			{
				float days = (_settlement.NextRestockTick - Find.TickManager.TicksGame).TicksToDays();
				Widgets.Label(labelRect, "NextTraderRestock".Translate(days.ToString("0.0")));
				currentTopHeight += LabelHeight;
			}

			Text.Anchor = TextAnchor.UpperLeft;
			inRect.yMin += 64f + currentTopHeight;

			if(CanShowInventory)
			{
				Widgets.DrawMenuSection(inRect);
				TabDrawer.DrawTabs(inRect, _tabs, 2);
				inRect = inRect.ContractedBy(17f);
			}

			GUI.BeginGroup(inRect);
			Rect rect2 = inRect.AtZero();
			DoBottomButtons(rect2);

			Rect outRect = rect2;
			outRect.yMax -= 65f;

			if(CanShowInventory)
			{
				int itemCount = _categoryItems.Count;
				if(itemCount > 0)
				{
					float maxHeight = itemCount * 24f;
					float currentHeight = 0f;
					Rect viewRect = new Rect(0f, 0f, outRect.width - 16f, maxHeight);
					Widgets.BeginScrollView(outRect, ref _scrollPosition, viewRect);
					float min = _scrollPosition.y - LabelHeight;
					float max = _scrollPosition.y + outRect.height;
					for(int index = 0; index < itemCount; ++index)
					{
						if(currentHeight > min && currentHeight < max)
						{
							Widgets.DefLabelWithIcon(new Rect(0f, currentHeight, viewRect.width, LabelHeight), _categoryItems[index]);
						}
						currentHeight += LabelHeight;
					}
					Widgets.EndScrollView();
				}
				else
				{
					Widgets.NoneLabel(0f, outRect.width);
				}
			}
			else
			{
				Text.Font = GameFont.Medium;
				Text.Anchor = TextAnchor.MiddleCenter;

				Rect viewRect = new Rect(0f, 0f, outRect.width - 16f, LabelHeight * 3f);
				Widgets.Label(new Rect(0f, 0f, viewRect.width, LabelHeight), "RequirementsNeedToBeFulfilled".Translate());
				float currentHeight = LabelHeight;
				if(!_visitRequirementFulfilled)
				{
					Widgets.Label(new Rect(0f, currentHeight, viewRect.width, LabelHeight), "SettlementNeedsVisit".Translate());
					currentHeight += LabelHeight;
				}
				if(!_commsConsoleRequirementFulfilled)
				{
					Widgets.Label(new Rect(0f, currentHeight, viewRect.width, LabelHeight), "CommsConsoleRequired".Translate());
				}

				Text.Anchor = TextAnchor.UpperLeft;
			}
			GUI.EndGroup();
		}

		#endregion

		#region Private Methods

		private void DoBottomButtons(Rect rect)
		{
			if(Widgets.ButtonText(new Rect(rect.width / 2f - BottomButtonSize.x / 2f, rect.height - 55f, BottomButtonSize.x, BottomButtonSize.y), "CloseButton".Translate()))
			{
				Close();
			}
		}

		private void CalculateTabs()
		{
			List<ThingCategoryDef> defsListForReading = DefDatabase<ThingCategoryDef>.AllDefsListForReading;
			for(int i = 0, length = defsListForReading.Count; i < length; i++)
			{
				ThingCategoryDef category = defsListForReading[i];
				if(category.parent == ThingCategoryDefOf.Root && AnyTraderWillEverTrade(category))
				{
					if(_currentCategory == null)
					{
						_currentCategory = category;
						SetBuyableItemsInCategory(_currentCategory);
					}
					_tabs.Add(new TabRecord(category.LabelCap, () =>
					{
						_currentCategory = category;
						SetBuyableItemsInCategory(_currentCategory);
					}, () => _currentCategory == category));
				}
			}
			_tabs.Add(new TabRecord("PawnsTabShort".Translate(), (() =>
			{
				_currentCategory = null;
				SetBuyableItemsInCategory(null);
			}), _currentCategory == null));
		}

		private bool AnyTraderWillEverTrade(ThingCategoryDef thingCategory)
		{
			List<ThingDef> thingDefs = DefDatabase<ThingDef>.AllDefsListForReading;
			for(int i = 0, iLength = thingDefs.Count; i < iLength; i++)
			{
				ThingDef thingDef = thingDefs[i];
				if(thingDef.IsWithinCategory(thingCategory))
				{
					List<TraderKindDef> traderDefs = DefDatabase<TraderKindDef>.AllDefsListForReading;
					for(int j = 0, jLength = traderDefs.Count; j < jLength; j++)
					{
						if(traderDefs[j].WillTrade(thingDef))
						{
							return true;
						}
					}
				}
			}
			return false;
		}

		private void SetBuyableItemsInCategory(ThingCategoryDef categoryDef)
		{
			_categoryItems.Clear();

			foreach(Thing thing in _settlement.Goods)
			{
				// Special exception for when the category is null, this means that we want to see pawns
				if(categoryDef == null && thing.def.category == ThingCategory.Pawn)
				{
					_categoryItems.Add(thing.def);
				}
				else
				{
					if(!_categoryItems.Contains(thing.def) && thing.def.IsWithinCategory(categoryDef))
					{
						_categoryItems.Add(thing.def);
					}
				}
			}
		}

		#endregion
	}
}